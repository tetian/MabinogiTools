﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;

namespace MyKnights.Entity
{
    [XmlRoot(ElementName = "Knight")]
    public class Knight
    {

        [XmlAttribute("Name")]
        public string Name { get; set; } = string.Empty;

        [XmlAttribute("WikiUrl")]
        public string WikiUrl { get; set; } = string.Empty;

        [XmlAttribute("BaseKey")]
        public string BaseKey { get; set; }

        [XmlArray("Topics"), XmlArrayItem("Topic")]
        public List<Topic> Topics { get; set; } = new List<Topic>();

        [XmlIgnore]
        public int CurrentTopic { get; set; } = 0;
        [XmlIgnore]
        public Dictionary<int, Topic> TopicsDictionary = new Dictionary<int, Topic>();
        /// <summary>
        /// 特殊话题
        /// </summary>
        [XmlIgnore]
        public string SpecialTopics { get; set; }

        public void AddTopic(Topic topic)
        {
            Topics.Add(topic);
        }

        public void LinkTopics()
        {
            foreach (Topic topic in Topics)
            {
                TopicsDictionary.Add(topic.Index, topic);
            }

            foreach (KeyValuePair<int, Topic> item in TopicsDictionary)
            {
                if (1 == item.Value.Index)
                {
                    item.Value.SetPrevious(TopicsDictionary[TopicsDictionary.Count]);
                    item.Value.SetNext(TopicsDictionary[2]);
                }
                else if (TopicsDictionary.Count == item.Value.Index)
                {
                    item.Value.SetPrevious(TopicsDictionary[TopicsDictionary.Count - 1]);
                    item.Value.SetNext(TopicsDictionary[1]);
                    item.Value.IsLast = true;
                }
                else
                {
                    item.Value.SetPrevious(TopicsDictionary[item.Value.Index - 1]);
                    item.Value.SetNext(TopicsDictionary[item.Value.Index + 1]);
                }
            }
            foreach (Topic topic in Topics)
            {
                if (topic.Next == null || topic.Previous == null)
                {
                    System.Diagnostics.Debug.WriteLine("Find null:" + topic.DisplayTextWithKeyword);
                }
            }
        }
    }
}
