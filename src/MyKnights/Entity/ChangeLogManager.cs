﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TUtil;

namespace MyKnights.Entity
{
    [XmlRoot(ElementName = "ChangeLog")]
    public class ChangeLogManager
    {
        [XmlArray("Logs"), XmlArrayItem("Log")]
        public List<ChangeLog> Logs { get; set; } = new List<ChangeLog>();

        [XmlIgnore]
        private bool _isSaving;

        public void Add(ChangeLog log)
        {
            Logs.Add(log);
            SaveFile();
        }

        public void SaveFile()
        {
            if (_isSaving) return;
            _isSaving = true;
            XmlHelper.SerializerToFile(this, Const.Path.LogXmlFile);
            _isSaving = false;
        }

        public void Clear()
        {
            Logs.Clear();
            SaveFile();
        }

        public static ChangeLogManager LoadFromFile()
        {
            return XmlHelper.XmlDeserializeFromFile<ChangeLogManager>(Const.Path.LogXmlFile);
        }

        public static ChangeLogManager Initialize()
        {
            ChangeLogManager cl = File.Exists(Const.Path.LogXmlFile) ? LoadFromFile() : new ChangeLogManager();
            return cl;
        }

    }
}
