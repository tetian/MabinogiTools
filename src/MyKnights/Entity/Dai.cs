﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyKnights.Entity
{

    //    条件 任务  训练 游戏  料理 时尚  恋爱
    //lv2 3/150			○		×	×
    //lv2 51/150					○	
    //lv2 120/150			×	×		○
    //lv3 17/100			○			
    //lv3 51/100		○			×	×
    //lv3 81/100	○					
    //lv4 30/200		○	×	×		
    //lv4 90/200					○	
    //lv4 160/200	×	×				○


    /// <summary>
    /// 戴伊
    /// </summary>
    public class Dai : Knight
    {
        public Dai()
        {
            Name = "戴伊";
            BaseKey = Key;
            WikiUrl = "https://wiki.mabinogiworld.com/view/Dai";

            SpecialTopics = "特殊话题:\n"
               + "lv2：3/150 " + Const.TopicName.Game + "\n"
               + "lv2：51/150 " + Const.TopicName.Fashion + "\n"
               + "lv2：120/150 " + Const.TopicName.Love + "\n"
               + "lv3：17/100 " + Const.TopicName.Game + "\n"
               + "lv3：51/100 " + Const.TopicName.Train + "\n"
               + "lv3：81/100 " + Const.TopicName.Quest + "\n"
               + "lv4：30/200 " + Const.TopicName.Train + "\n"
               + "lv4：90/200 " + Const.TopicName.Fashion + "\n"
               + "lv4：160/200 " + Const.TopicName.Love;
        }

        public static string Key = "Dai";

        public void InitTopics()
        {
            Topics.Clear();
            AddTopic(Topic.Game(1, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Love(2, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Game(3, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(4, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Train(5, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(6, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(7, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(8, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(9, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Cooking(10, Const.KeywordName.Dai_RM));

            AddTopic(Topic.Game(11, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Train(12, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(13, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Love(14, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Quest(15, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Love(16, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Fashion(17, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(18, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(19, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Cooking(20, Const.KeywordName.Dai_RM));

            AddTopic(Topic.Fashion(21, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(22, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(23, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(24, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(25, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Cooking(26, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(27, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(28, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Cooking(29, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Quest(30, Const.KeywordName.Dai_CH));

            AddTopic(Topic.Love(31, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(32, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Cooking(33, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(34, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Fashion(35, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(36, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Cooking(37, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(38, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(39, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Train(40, Const.KeywordName.Dai_RM));

            AddTopic(Topic.Game(41, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(42, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Game(43, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Love(44, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Quest(45, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Love(46, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Game(47, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Love(48, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Game(49, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Fashion(50, Const.KeywordName.Dai_KZ));

            AddTopic(Topic.Cooking(51, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(52, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Fashion(53, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Game(54, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(55, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Train(56, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Train(57, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(58, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(59, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(60, Const.KeywordName.Dai_RM));

            AddTopic(Topic.Game(61, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(62, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Fashion(63, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Train(64, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Cooking(65, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Fashion(66, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(67, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Fashion(68, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(69, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Fashion(70, Const.KeywordName.Dai_KZ));

            AddTopic(Topic.Game(71, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Train(72, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Game(73, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Quest(74, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Cooking(75, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Game(76, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Train(77, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Game(78, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(79, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(80, Const.KeywordName.Dai_RM));

            AddTopic(Topic.Quest(81, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Love(82, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Train(83, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Love(84, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(85, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Quest(86, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Fashion(87, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Love(88, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(89, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Quest(90, Const.KeywordName.Dai_RM));

            AddTopic(Topic.Fashion(91, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Cooking(92, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Game(93, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Fashion(94, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Cooking(95, Const.KeywordName.Dai_RM));
            AddTopic(Topic.Love(96, Const.KeywordName.Dai_KZ));
            AddTopic(Topic.Fashion(97, Const.KeywordName.Dai_CH));
            AddTopic(Topic.Love(98, Const.KeywordName.Dai_CH, "96->97->98(1)"));
            AddTopic(Topic.Fashion(99, Const.KeywordName.Dai_KZ, "97->98->99(2)"));
        }
    }
}
