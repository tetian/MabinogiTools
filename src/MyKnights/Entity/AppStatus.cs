﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TUtil;

namespace MyKnights.Entity
{
    [XmlRoot(ElementName = "AppStatus")]
    public class AppStatus : TUtil.BaseAppStatus
    {
        public BaseWindowStatus MainWindowStatus { get; set; } = new BaseWindowStatus();

        public bool GridLogVisible { get; set; } = false;


    }
}
