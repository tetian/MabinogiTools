﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TUtil;

namespace MyKnights.Entity
{
    [XmlRoot("Knights")]
    public class Knights
    {
        [XmlElement("Elsie")]
        public Elsie Elsie { get; set; } = new Elsie();
        [XmlElement("Eirlys")]
        public Eirlys Eirlys { get; set; } = new Eirlys();
        [XmlElement("Kaour")]
        public Kaour Kaour { get; set; } = new Kaour();
        [XmlElement("Dai")]
        public Dai Dai { get; set; } = new Dai();

        public void SaveToFile()
        {
            XmlHelper.SerializerToFile(this, Const.Path.TopicsXml);
        }

        public void LinkKnightsTopic()
        {
            Elsie.LinkTopics();
            Eirlys.LinkTopics();
            Kaour.LinkTopics();
            Dai.LinkTopics();
        }

        public static Knights LoadFromDefault()
        {
            Knights kns = new Knights();
            kns.Elsie.InitTopics();
            kns.Eirlys.InitTopics();
            kns.Dai.InitTopics();
            kns.Kaour.InitTopics();
            return kns;
        }

        public static Knights LoadFromFile()
        {
            return XmlHelper.XmlDeserializeFromFile<Knights>(Const.Path.TopicsXml);
        }

        public static Knights Initialize()
        {
            Knights ks = File.Exists(Const.Path.TopicsXml) ? LoadFromFile() : LoadFromDefault();
            ks.LinkKnightsTopic();
            return ks;
        }
    }
}
