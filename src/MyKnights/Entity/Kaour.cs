﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyKnights.Entity
{

    //    条件 任务  训练 游戏  料理 时尚  恋爱 备注
    //lv2 2/300		○					
    //lv2　160/300			○				
    //lv2　270/300	○						
    //lv3　18/100		○					
    //lv3　40/100		○					
    //lv3　80/100	○						
    //lv4　15/150					－	○	
    //lv4 85/150		○					
    //lv4 140/150			○				


    /// <summary>
    /// 卡沃尔
    /// </summary>
    public class Kaour : Knight
    {
        public Kaour()
        {
            Name = "卡沃尔";
            BaseKey = Key;
            WikiUrl = "https://wiki.mabinogiworld.com/view/Kaour";

            SpecialTopics = "特殊话题:\n"
                + "lv2：2/300 " + Const.TopicName.Train + "\n"
                + "lv2：160/300 " + Const.TopicName.Game + "\n"
                + "lv2：270/300 " + Const.TopicName.Quest + "\n"
                + "lv3：18/100 " + Const.TopicName.Train + "\n"
                + "lv3：40/100 " + Const.TopicName.Train + "\n"
                + "lv3：80/100 " + Const.TopicName.Quest + "\n"
                + "lv4：15/200 " + Const.TopicName.Love + "\n"
                + "lv4：85/200 " + Const.TopicName.Train + "\n"
                + "lv4：140/200 " + Const.TopicName.Game;
        }

        public static string Key = "Kaour";

        public void InitTopics()
        {
            Topics.Clear();
            AddTopic(Topic.Game(1));
            AddTopic(Topic.Game(2));
            AddTopic(Topic.Train(3));
            AddTopic(Topic.Cooking(4));
            AddTopic(Topic.Game(5));
            AddTopic(Topic.Love(6));
            AddTopic(Topic.Train(7));
            AddTopic(Topic.Cooking(8));
            AddTopic(Topic.Love(9));
            AddTopic(Topic.Cooking(10));

            AddTopic(Topic.Quest(11));
            AddTopic(Topic.Quest(12));
            AddTopic(Topic.Fashion(13));
            AddTopic(Topic.Love(14));
            AddTopic(Topic.Cooking(15));
            AddTopic(Topic.Fashion(16));
            AddTopic(Topic.Love(17));
            AddTopic(Topic.Quest(18));
            AddTopic(Topic.Quest(19));
            AddTopic(Topic.Quest(20));

            AddTopic(Topic.Fashion(21));
            AddTopic(Topic.Love(22));
            AddTopic(Topic.Quest(23));
            AddTopic(Topic.Fashion(24));
            AddTopic(Topic.Game(25));
            AddTopic(Topic.Quest(26));
            AddTopic(Topic.Fashion(27));
            AddTopic(Topic.Train(28));
            AddTopic(Topic.Love(29));
            AddTopic(Topic.Quest(30));

            AddTopic(Topic.Quest(31));
            AddTopic(Topic.Game(32));
            AddTopic(Topic.Love(33));
            AddTopic(Topic.Quest(34));
            AddTopic(Topic.Fashion(35));
            AddTopic(Topic.Quest(36));
            AddTopic(Topic.Train(37));
            AddTopic(Topic.Train(38));
            AddTopic(Topic.Train(39));
            AddTopic(Topic.Game(40));

            AddTopic(Topic.Train(41));
            AddTopic(Topic.Love(42));
            AddTopic(Topic.Fashion(43));
            AddTopic(Topic.Cooking(44));
            AddTopic(Topic.Train(45));
            AddTopic(Topic.Game(46));
            AddTopic(Topic.Fashion(47));
            AddTopic(Topic.Love(48));
            AddTopic(Topic.Game(49));
            AddTopic(Topic.Love(50));

            AddTopic(Topic.Cooking(51));
            AddTopic(Topic.Quest(52));
            AddTopic(Topic.Game(53));
            AddTopic(Topic.Fashion(54));
            AddTopic(Topic.Cooking(55));
            AddTopic(Topic.Cooking(56));
            AddTopic(Topic.Cooking(57));
            AddTopic(Topic.Fashion(58));
            AddTopic(Topic.Fashion(59));
            AddTopic(Topic.Game(60));

            AddTopic(Topic.Game(61));
            AddTopic(Topic.Train(62));
            AddTopic(Topic.Fashion(63));
            AddTopic(Topic.Train(64));
            AddTopic(Topic.Quest(65));
            AddTopic(Topic.Train(66));
            AddTopic(Topic.Cooking(67));
            AddTopic(Topic.Love(68));
            AddTopic(Topic.Fashion(69));
            AddTopic(Topic.Train(70));

            AddTopic(Topic.Game(71));
            AddTopic(Topic.Fashion(72));
            AddTopic(Topic.Love(73));
            AddTopic(Topic.Train(74));
            AddTopic(Topic.Quest(75));
            AddTopic(Topic.Quest(76));
            AddTopic(Topic.Cooking(77));
            AddTopic(Topic.Quest(78));
            AddTopic(Topic.Quest(79));
            AddTopic(Topic.Fashion(80));

            AddTopic(Topic.Cooking(81));
            AddTopic(Topic.Cooking(82));
            AddTopic(Topic.Train(83));
            AddTopic(Topic.Fashion(84));
            AddTopic(Topic.Cooking(85));
            AddTopic(Topic.Love(86));
            AddTopic(Topic.Quest(87));
            AddTopic(Topic.Fashion(88));
            AddTopic(Topic.Love(89));
            AddTopic(Topic.Cooking(90));

            AddTopic(Topic.Love(91));
            AddTopic(Topic.Fashion(92));
            AddTopic(Topic.Love(93));
            AddTopic(Topic.Train(94));
            AddTopic(Topic.Quest(95));
            AddTopic(Topic.Cooking(96));
            AddTopic(Topic.Train(97));
            AddTopic(Topic.Love(98, "", "96->97->98(1)"));
            AddTopic(Topic.Fashion(99, "", "97->98->99(2)"));
        }
    }
}
