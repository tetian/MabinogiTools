﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyKnights.Entity
{

    /// <summary>
    /// 艾尔西
    /// </summary>
    public class Elsie : Knight
    {
        public Elsie()
        {
            BaseKey = Key;
            Name = "艾尔西";
            WikiUrl = "https://wiki.mabinogiworld.com/view/Elsie";

            SpecialTopics = "特殊话题:\n"
                + "lv2：2/300 " + Const.TopicName.Train + "\n"
                + "lv2：140/300 " + Const.TopicName.Game + "\n"
                + "lv2：250/300 " + Const.TopicName.Cooking + "\n"
                + "lv3：25/100 " + Const.TopicName.Love + "\n"
                + "lv3：50/100 " + Const.TopicName.Game + "\n"
                + "lv3：80/100 " + Const.TopicName.Quest + "\n"
                + "lv4：50/200 " + Const.TopicName.Train + "\n"
                + "lv4：100/200 " + Const.TopicName.Game + "\n"
                + "lv4：170/200 " + Const.TopicName.Cooking;
        }

        public static string Key = "Elsie";

        public void InitTopics()
        {
            Topics.Clear();

            AddTopic(Topic.Cooking(1));
            AddTopic(Topic.Game(2));
            AddTopic(Topic.Fashion(3));
            AddTopic(Topic.Love(4));
            AddTopic(Topic.Fashion(5));
            AddTopic(Topic.Game(6));
            AddTopic(Topic.Love(7));
            AddTopic(Topic.Love(8));
            AddTopic(Topic.Game(9));
            AddTopic(Topic.Quest(10));

            AddTopic(Topic.Train(11));
            AddTopic(Topic.Game(12));
            AddTopic(Topic.Love(13));
            AddTopic(Topic.Game(14));
            AddTopic(Topic.Train(15));
            AddTopic(Topic.Game(16));
            AddTopic(Topic.Quest(17));
            AddTopic(Topic.Cooking(18));
            AddTopic(Topic.Game(19));
            AddTopic(Topic.Game(20));

            AddTopic(Topic.Game(21));
            AddTopic(Topic.Love(22));
            AddTopic(Topic.Game(23));
            AddTopic(Topic.Cooking(24));
            AddTopic(Topic.Game(25));
            AddTopic(Topic.Love(26));
            AddTopic(Topic.Quest(27));
            AddTopic(Topic.Fashion(28));
            AddTopic(Topic.Game(29));
            AddTopic(Topic.Game(30));

            AddTopic(Topic.Game(31));
            AddTopic(Topic.Love(32));
            AddTopic(Topic.Cooking(33));
            AddTopic(Topic.Love(34));
            AddTopic(Topic.Train(35));
            AddTopic(Topic.Love(36));
            AddTopic(Topic.Game(37));
            AddTopic(Topic.Cooking(38));
            AddTopic(Topic.Quest(39));
            AddTopic(Topic.Train(40));

            AddTopic(Topic.Game(41));
            AddTopic(Topic.Game(42));
            AddTopic(Topic.Quest(43));
            AddTopic(Topic.Love(44));
            AddTopic(Topic.Cooking(45));
            AddTopic(Topic.Love(46));
            AddTopic(Topic.Cooking(47));
            AddTopic(Topic.Game(48));
            AddTopic(Topic.Game(49));
            AddTopic(Topic.Love(50));

            AddTopic(Topic.Game(51));
            AddTopic(Topic.Train(52));
            AddTopic(Topic.Love(53));
            AddTopic(Topic.Game(54));
            AddTopic(Topic.Quest(55));
            AddTopic(Topic.Game(56));
            AddTopic(Topic.Game(57));
            AddTopic(Topic.Game(58));
            AddTopic(Topic.Fashion(59));
            AddTopic(Topic.Love(60));

            AddTopic(Topic.Game(61));
            AddTopic(Topic.Game(62));
            AddTopic(Topic.Quest(63));
            AddTopic(Topic.Train(64));
            AddTopic(Topic.Love(65));
            AddTopic(Topic.Love(66));
            AddTopic(Topic.Game(67));
            AddTopic(Topic.Game(68));
            AddTopic(Topic.Game(69));
            AddTopic(Topic.Fashion(70));

            AddTopic(Topic.Game(71));
            AddTopic(Topic.Train(72));
            AddTopic(Topic.Cooking(73));
            AddTopic(Topic.Game(74));
            AddTopic(Topic.Fashion(75));
            AddTopic(Topic.Game(76));
            AddTopic(Topic.Fashion(77));
            AddTopic(Topic.Love(78));
            AddTopic(Topic.Quest(79));
            AddTopic(Topic.Game(80));

            AddTopic(Topic.Game(81));
            AddTopic(Topic.Love(82));
            AddTopic(Topic.Train(83));
            AddTopic(Topic.Game(84));
            AddTopic(Topic.Game(85));
            AddTopic(Topic.Fashion(86));
            AddTopic(Topic.Love(87));
            AddTopic(Topic.Quest(88));
            AddTopic(Topic.Quest(89));
            AddTopic(Topic.Fashion(90));

            AddTopic(Topic.Fashion(91));
            AddTopic(Topic.Fashion(92));
            AddTopic(Topic.Fashion(93));
            AddTopic(Topic.Love(94));
            AddTopic(Topic.Game(95));
            AddTopic(Topic.Game(96));
            AddTopic(Topic.Quest(97));
            AddTopic(Topic.Cooking(98, string.Empty, "96->97->98(1)"));
            AddTopic(Topic.Quest(99, string.Empty, "97->98->99(2)"));
        }
    }
}
