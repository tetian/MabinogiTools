﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyKnights.Entity
{
    [XmlRoot(ElementName = "Item")]
    public class Profile
    {
        [XmlAttribute("Name")]
        public string Name { get; set; } = string.Empty;
        [XmlAttribute("Selected")]
        public bool Selected { get; set; }

        [XmlElement("Elsie")]
        public int Elsie { get; set; }
        [XmlElement("Eirlys")]
        public int Eirlys { get; set; }
        [XmlElement("Dai")]
        public int Dai { get; set; }
        [XmlElement("Kaour")]
        public int Kaour { get; set; }

        [XmlElement("KnightVisibility")]
        public KnightVisibility KnightVisibility { get; set; } = new KnightVisibility();

        public override string ToString()
        {
            return Name;
        }
    }
}
