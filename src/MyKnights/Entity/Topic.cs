﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MyKnights.Entity
{
    [XmlRoot(ElementName = "Topic")]
    public class Topic
    {
        public Topic()
        {

        }

        public Topic(int index, string name, string keyword, string remark)
        {
            Index = index;
            Name = name;
            Keyword = keyword;
            Remark = remark;
        }

        [XmlAttribute("Index")]
        public int Index { get; set; }
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("Keyword")]
        public string Keyword { get; set; }
        [XmlAttribute("Remark")]
        public string Remark { get; set; }


        [XmlIgnore]
        public bool IsLast { get; set; }

        //01 XX [XX]
        [XmlIgnore]
        public string DisplayText => string.Format(Index < 10 ? "0{0}" : "{0}", Index)
            + " " + Name;

        [XmlIgnore]
        public string DisplayTextWithKeyword => string.Format(Index < 10 ? "0{0}" : "{0}", Index)
            + " " + Name + (string.IsNullOrWhiteSpace(Keyword) ? string.Empty : " [" + Keyword + "]");

        [XmlIgnore]
        public string ToolTip => string.Format(Index < 10 ? "0{0}" : "{0}", Index)
            + " " + Name
            + (string.IsNullOrWhiteSpace(Keyword) ? string.Empty : "\n关键字:" + Keyword)
            + (string.IsNullOrWhiteSpace(Remark) ? string.Empty : "\n" + Remark);

        [XmlIgnore]
        public Topic Previous { get; private set; }

        public void SetPrevious(Topic topic)
        {
            Previous = topic;
        }

        [XmlIgnore]
        public Topic Next { get; private set; }

        public void SetNext(Topic topic)
        {
            Next = topic;
        }

        public override string ToString()
        {
            return DisplayText;
        }

        public static Topic Quest(int index, string keyword = "", string remark = "")
        {
            return new Topic(index, Const.TopicName.Quest, keyword, remark);
        }

        public static Topic Train(int index, string keyword = "", string remark = "")
        {
            return new Topic(index, Const.TopicName.Train, keyword, remark);
        }

        public static Topic Game(int index, string keyword = "", string remark = "")
        {
            return new Topic(index, Const.TopicName.Game, keyword, remark);
        }

        public static Topic Cooking(int index, string keyword = "", string remark = "")
        {
            return new Topic(index, Const.TopicName.Cooking, keyword, remark);
        }

        public static Topic Fashion(int index, string keyword = "", string remark = "")
        {
            return new Topic(index, Const.TopicName.Fashion, keyword, remark);
        }

        public static Topic Love(int index, string keyword = "", string remark = "")
        {
            return new Topic(index, Const.TopicName.Love, keyword, remark);
        }
    }
}
