﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;

namespace MyKnights.Entity
{
    //    条件 任务  训练 游戏  料理 时尚  恋爱 备注
    //lv1　3/70		○	×		×		第二个/任一都可以
    //lv120/70	×	×				○	任一都可以
    //lv150/70		○		×	×		无选择
    //lv210/150		×			○		无选择
    //lv2 50/150		○	×				我…… （重复很多次）
    //lv2 120/150	○		×				理解
    //lv3 20/100	○				×		
    //lv3 40/100		×		○			一直右
    //lv3 80/100	×		○				
    //lv4 6/50	×		×			○	不是我们的故事


    /// <summary>
    /// 艾尔莉丝
    /// </summary>
    public class Eirlys : Knight
    {
        public Eirlys()
        {
            Name = "艾尔莉丝";
            BaseKey = Key;
            WikiUrl = "https://wiki.mabinogiworld.com/view/Eirlys";

            SpecialTopics = "特殊话题:\n"
                + "lv1：3/70 " + Const.TopicName.Train + "\n"
                + "lv1：20/70 " + Const.TopicName.Love + "\n"
                + "lv1：50/70 " + Const.TopicName.Train + "\n"
                + "lv2：10/150 " + Const.TopicName.Fashion + "\n"
                + "lv2：50/150 " + Const.TopicName.Train + "\n"
                + "lv2：120/150 " + Const.TopicName.Quest + "\n"
                + "lv3：20/100 " + Const.TopicName.Quest + "\n"
                + "lv3：40/100 " + Const.TopicName.Cooking + "\n"
                + "lv3：80/100 " + Const.TopicName.Game + "\n"
                + "lv4：6/50 " + Const.TopicName.Love;
        }

        public static string Key = "Eirlys";

        public void InitTopics()
        {
            Topics.Clear();
            AddTopic(Topic.Quest(1, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Game(2, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Cooking(3, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Game(4, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Quest(5, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Train(6, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Quest(7, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Quest(8, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(9, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Quest(10, Const.KeywordName.Eirlys_YX));

            AddTopic(Topic.Train(11, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Fashion(12, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Train(13, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Train(14, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Cooking(15, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Love(16, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(17, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Fashion(18, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Quest(19, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Train(20, Const.KeywordName.Eirlys_YX));

            AddTopic(Topic.Love(21, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Fashion(22, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Love(23, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Love(24, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Cooking(25, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Train(26, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Cooking(27, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Game(28, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Fashion(29, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Fashion(30, Const.KeywordName.Eirlys_XL));

            AddTopic(Topic.Quest(31, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Quest(32, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Quest(33, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(34, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Train(35, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Train(36, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Quest(37, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Cooking(38, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Quest(39, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Quest(40, Const.KeywordName.Eirlys_LL));

            AddTopic(Topic.Cooking(41, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Game(42, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Game(43, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Train(44, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Cooking(45, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Game(46, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Cooking(47, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Cooking(48, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Quest(49, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Cooking(50, Const.KeywordName.Eirlys_LA));

            AddTopic(Topic.Train(51, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Quest(52, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Quest(53, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Train(54, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Train(55, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Love(56, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Love(57, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Quest(58, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(59, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Cooking(60, Const.KeywordName.Eirlys_SS));

            AddTopic(Topic.Game(61, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Fashion(62, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Train(63, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(64, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Cooking(65, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Quest(66, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Train(67, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Quest(68, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Quest(69, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Quest(70, Const.KeywordName.Eirlys_LL));

            AddTopic(Topic.Game(71, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Train(72, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(73, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Game(74, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(75, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Quest(76, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Train(77, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Quest(78, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Train(79, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Fashion(80, Const.KeywordName.Eirlys_LL));

            AddTopic(Topic.Quest(81, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Train(82, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Cooking(83, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Quest(84, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Fashion(85, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Game(86, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Train(87, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Train(88, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Cooking(89, Const.KeywordName.Eirlys_RW));
            AddTopic(Topic.Cooking(90, Const.KeywordName.Eirlys_LA));

            AddTopic(Topic.Quest(91, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Train(92, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Cooking(93, Const.KeywordName.Eirlys_YX));
            AddTopic(Topic.Quest(94, Const.KeywordName.Eirlys_LA));
            AddTopic(Topic.Quest(95, Const.KeywordName.Eirlys_SS));
            AddTopic(Topic.Love(96, Const.KeywordName.Eirlys_LL));
            AddTopic(Topic.Cooking(97, Const.KeywordName.Eirlys_XL));
            AddTopic(Topic.Cooking(98, Const.KeywordName.Eirlys_LA, "96->97->98(1)"));
            AddTopic(Topic.Quest(99, Const.KeywordName.Eirlys_XL, "97->98->99(2)"));
        }
    }
}
