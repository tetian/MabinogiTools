﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyKnights.Entity
{
    //不需要实现INotifyPropertyChanged
    [XmlRoot(ElementName = "Log")]
    public class ChangeLog
    {
        [XmlAttribute("FromIndex")]
        public int FromIndex { get; set; }
        [XmlAttribute("FromTopic")]
        public string FromTopic { get; set; }
        [XmlIgnore]
        public string FomartFrom => (FromIndex < 10 ? "0" + FromIndex : FromIndex.ToString()) + " " + FromTopic;

        [XmlAttribute("ToIndex")]
        public int ToIndex { get; set; }
        [XmlAttribute("ToTopic")]
        public string ToTopic { get; set; }
        [XmlIgnore]
        public string FomartTo => (ToIndex < 10 ? "0" + ToIndex : ToIndex.ToString()) + " " + ToTopic;

        [XmlAttribute("ProfileName")]
        public string ProfileName { get; set; }

        [XmlAttribute("KnightName")]
        public string KnightName { get; set; }

        [XmlAttribute("ChangeTime")]
        public DateTime ChangeTime { get; set; }

        public override string ToString()
        {
            return DisplayText;
        }

        [XmlIgnore]
        public string DisplayText => $"[{FomartFrom}->{FomartTo}][{KnightName} {ProfileName} {ChangeTime:MM-dd HH:mm:ss}]";
    }
}
