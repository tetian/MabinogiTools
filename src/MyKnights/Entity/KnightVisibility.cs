﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyKnights.Entity
{
    [XmlRoot(ElementName = "KnightVisibility")]
    public class KnightVisibility
    {
        public bool Elsie { get; set; } = true;
        public bool Eirlys { get; set; } = true;
        public bool Dai { get; set; } = true;
        public bool Kaour { get; set; } = true;
    }
}
