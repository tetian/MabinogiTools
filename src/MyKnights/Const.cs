﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MyKnights
{
    class Const
    {

        public class UI
        {
            public const double MainWindowHeight = 362.0;
            public const double UIKHeight = 94.0;
        }

        public class TopicName
        {
            public const string Quest = "任务";
            public const string Train = "修炼";
            public const string Game = "游戏";
            public const string Cooking = "料理";
            public const string Fashion = "时尚";
            public const string Love = "恋爱";
        }

        public class KeywordName
        {
            /// <summary>
            /// 枯燥
            /// </summary>
            public const string Dai_KZ = "枯燥";
            /// <summary>
            /// 吃喝
            /// </summary>
            public const string Dai_CH = "吃喝";
            /// <summary>
            /// 肉麻
            /// </summary>
            public const string Dai_RM = "肉麻";
            /// <summary>
            /// 时尚
            /// </summary>
            public const string Eirlys_SS = "时尚";
            /// <summary>
            /// 任务
            /// </summary>
            public const string Eirlys_RW = "任务";
            /// <summary>
            /// 修炼
            /// </summary>
            public const string Eirlys_XL = "修炼";
            /// <summary>
            /// 料理
            /// </summary>
            public const string Eirlys_LL = "料理";
            /// <summary>
            /// 游戏
            /// </summary>
            public const string Eirlys_YX = "游戏";
            /// <summary>
            /// 恋爱
            /// </summary>
            public const string Eirlys_LA = "恋爱";
        }

        public class Path
        {
            public static string DataDir => BaseDir + @"mt_data\";
            public const string UserDataFileName = "mk_userdata.xml";
            public const string TopicsFileName = "mk_topics.xml";
            public const string ErrorFileName = "mk_error.txt";
            //public const string LogFileName = "mk_log.txt";
            public const string LogXmlFileName = "mk_log.xml";

            public static string BaseDir => AppDomain.CurrentDomain.BaseDirectory;

            public static string UserDataXml => System.IO.Path.Combine(DataDir, UserDataFileName);
            public static string TopicsXml => System.IO.Path.Combine(DataDir, TopicsFileName);
            public static string ErrorFile = System.IO.Path.Combine(DataDir, ErrorFileName);
            //public static string LogFile = System.IO.Path.Combine(DataDir, LogFileName);
            public static string LogXmlFile = System.IO.Path.Combine(DataDir, LogXmlFileName);
        }

    }
}
