﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyKnights.Entity;

namespace MyKnights
{
    public interface IMainWindow
    {
        void ApplyAppStatus(AppStatus appStatus);

        void InitProfile(UserData userData);

        void LoadTopics(Knights kinghts);

        void LoadKnightVisibility(KnightVisibility knightVisibility);

        void RefreshLogs();
    }
}
