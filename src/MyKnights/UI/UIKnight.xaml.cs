﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyKnights.Entity;
using MyKnights.Styles;

namespace MyKnights.UI
{
    /// <summary>
    /// UIKnight.xaml 的交互逻辑
    /// </summary>
    public partial class UIKnight : UserControl
    {



        public static Brush GetFeatureBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(FeatureBrushProperty);
        }

        public static void SetFeatureBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(FeatureBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for FeatureBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FeatureBrushProperty =
            DependencyProperty.RegisterAttached("FeatureBrush", typeof(Brush), typeof(UIKnight), new PropertyMetadata(Brushes.Transparent, OnFeatureBrushChanged));

        private static void OnFeatureBrushChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UIKnight ui = obj as UIKnight;
            if (ui == null) return;
            Storyboard sb = ui.FindResource("StoryboardListBoxItem") as Storyboard;
            if (sb != null)
            {
                ColorAnimation ca = sb.Children[0] as ColorAnimation;
                if (ca != null)
                {
                    Color to = ((SolidColorBrush)e.NewValue).Color;
                    Color from = Color.Subtract(Colors.White, to);
                    ca.From = from;
                    ca.To = to;
                }
            }

            ui.GridLeft.Background = e.NewValue as Brush;
        }



        public delegate void TopicChangedEvent(object sender, Topic fromTopic, Topic toTopic);
        /// <summary>
        /// topic改变事件
        /// </summary>
        public event TopicChangedEvent TopicChanged;

        public ImageSource FaceSource
        {
            get { return Face.Source; }
            set { Face.Source = value; }
        }

        public Knight Knight { get; set; }

        private bool _initialized;

        private Topic _selectedTopic;

        public UIKnight()
        {
            InitializeComponent();
        }

        public void LoadTopics(Knight knight)
        {
            _initialized = false;
            Knight = knight;
            LbTopics.Items.Clear();
            //Brush fcolor = GetFeatureBrush(this);
            foreach (var topic in knight.Topics)
            {
                var lbi = new ListBoxItem
                {
                    Style = Application.Current.Resources["CheckButtonListBoxItem"] as Style,
                    Tag = topic,
                    Content = topic.DisplayText,
                    ToolTip = topic.ToolTip,
                    FontSize = 4,
                };

                LbTopics.Items.Add(lbi);
                if (topic.Index.Equals(knight.CurrentTopic))
                {
                    lbi.IsSelected = true;
                    _selectedTopic = topic;
                }
            }
            ScrollSelectedItem();
            //tooltip
            Face.ToolTip = knight.Name;
            _initialized = true;
        }

        private void LbTopics_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //正在加载的时不触发事件
            if (!_initialized) return;
            if (e.AddedItems.Count <= 0) return;
            var toTopic = (e.AddedItems[0] as ListBoxItem)?.Tag as Topic;
            if (toTopic == null) return;
            TopicChanged?.Invoke(this, _selectedTopic, toTopic);
            _selectedTopic = toTopic;
            ScrollSelectedItem();
        }

        public void ScrollSelectedItem()
        {
            if (LbTopics.SelectedItems.Count > 0)
            {
                LbTopics.ScrollIntoView(LbTopics.Items[LbTopics.Items.Count - 1]);
                LbTopics.UpdateLayout();
                LbTopics.ScrollIntoView(LbTopics.SelectedItems[0]);
                Storyboard sb = (Storyboard)FindResource("StoryboardListBoxItem");
                sb.Begin(GridLeft);

                Storyboard sblist = (Storyboard)FindResource("StoryboardListBox");
                sblist.Begin(LbTopics);

                sblist.Begin(ButtonPlus3);
            }
        }

        private void ButtonLocation_OnClick(object sender, RoutedEventArgs e)
        {
            ScrollSelectedItem();
        }

        private void ButtonDetail_OnClick(object sender, RoutedEventArgs e)
        {
            KnightDetailWindow kdw = new KnightDetailWindow
            {
                Owner = Window.GetWindow(this),
                FaceSource = FaceSource
            };
            kdw.TopicChanged += Kdw_TopicChanged;
            kdw.LoadTopicsShow(LbTopics.Items, Knight);
            ScrollSelectedItem();
        }

        private void Kdw_TopicChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Topic topic = (e.AddedItems[0] as ListBoxItem)?.Tag as Topic;
                if (topic == null) return;
                foreach (ListBoxItem lbi in LbTopics.Items)
                {
                    var selTopic = lbi.Tag as Topic;
                    if (selTopic == null) continue;
                    if (topic.Index.Equals(selTopic.Index))
                    {
                        //触发LbTopics_OnSelectionChanged
                        lbi.IsSelected = true;
                        break;
                    }
                }
            }
        }

        private void ButtonScrollTop_OnClick(object sender, RoutedEventArgs e)
        {
            LbTopics.ScrollIntoView(LbTopics.Items[0]);
        }

        private void ButtonPlus3_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem lbi = LbTopics.SelectedItem as ListBoxItem;
            if (lbi == null) return;
            var tpc = lbi.Tag as Topic;
            int next_index = tpc.Index + 3;
            if (98 == next_index)
            {
                next_index = 1;
            }
            else if (99 == next_index)
            {
                next_index = 2;
            }
            else if (99 < next_index && 99 + 3 >= next_index)
            {
                //97->3
                //98->1+3
                //99->2+3
                next_index = next_index - 99 - 1 + 3;
            }
            else if (99 + 3 < next_index)
            {
                next_index = 3;
            }

            LbTopics.SelectedIndex = next_index - 1;
            if (97 == next_index)
            {
                LbTopics.ScrollIntoView(LbTopics.Items[0]);
            }
        }
    }
}
