﻿using MyKnights.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyKnights.UI
{
    /// <summary>
    /// ProfileEditWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ProfileEditWindow : Window
    {
        public enum Operation
        {
            Add,
            Edit,
            Delete
        }

        public Operation ProfileOperation { get; set; }

        private CenterHandler _centerHandler;

        public ProfileEditWindow()
        {
            InitializeComponent();
        }

        public void InitializeShow(Operation operation, CenterHandler centerHandler)
        {
            ProfileOperation = operation;
            _centerHandler = centerHandler;
            string topicName = centerHandler.CurrentProfile.Name;
            if (Operation.Add == ProfileOperation)
            {
                TBTitle.Text = "添加配置";
                TBProfile.Text = string.Empty;
            }
            else if (Operation.Edit == ProfileOperation)
            {
                TBTitle.Text = "编辑配置";
                TBProfile.Text = centerHandler.CurrentProfile.Name;
                CheckBoxElsie.IsChecked = centerHandler.CurrentProfile.KnightVisibility.Elsie;
                CheckBoxEirlys.IsChecked = centerHandler.CurrentProfile.KnightVisibility.Eirlys;
                CheckBoxKaour.IsChecked = centerHandler.CurrentProfile.KnightVisibility.Kaour;
                CheckBoxDai.IsChecked = centerHandler.CurrentProfile.KnightVisibility.Dai;
            }
            else if (Operation.Delete == ProfileOperation)
            {
                TBTitle.Text = "删除配置";
                TBProfile.Text = centerHandler.CurrentProfile.Name;
                TBProfile.IsReadOnly = true;
                CheckBoxElsie.IsEnabled = false;
                CheckBoxEirlys.IsEnabled = false;
                CheckBoxKaour.IsEnabled = false;
                CheckBoxDai.IsEnabled = false;
            }
            ShowDialog();
        }

        private void ButtonCancel_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ButtonOK_OnClick(object sender, RoutedEventArgs e)
        {
            Profile storedProfile = new Profile
            {
                Name = TBProfile.Text,
                KnightVisibility = new KnightVisibility
                {
                    Elsie = CheckBoxElsie.IsChecked.Value,
                    Eirlys = CheckBoxEirlys.IsChecked.Value,
                    Kaour = CheckBoxKaour.IsChecked.Value,
                    Dai = CheckBoxDai.IsChecked.Value
                }
            };
            _centerHandler.UpdateProfile(ProfileOperation, storedProfile);
            Close();
        }
    }
}
