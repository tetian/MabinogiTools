﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyKnights.Entity;

namespace MyKnights.UI
{
    /// <summary>
    /// KnightDetailWindow.xaml 的交互逻辑
    /// </summary>
    public partial class KnightDetailWindow : Window
    {
        public delegate void TopicChangedEvent(object sender, SelectionChangedEventArgs obj);
        /// <summary>
        /// topic改变事件
        /// </summary>
        public event TopicChangedEvent TopicChanged;

        public ImageSource FaceSource
        {
            get { return Face.Source; }
            set { Face.Source = value; }
        }

        private bool _initialized;
        private Knight _knight;

        public KnightDetailWindow()
        {
            InitializeComponent();
        }

        public void LoadTopicsShow(ItemCollection liCollection, Knight knight)
        {
            _initialized = false;
            LbTopics.Items.Clear();
            foreach (ListBoxItem topicItem in liCollection)
            {
                var topic = topicItem.Tag as Topic;
                if (topic == null) continue;
                var lbi = new ListBoxItem
                {
                    Style = Application.Current.Resources["CheckButtonListBoxItem"] as Style,
                    Tag = topic,
                    Content = topic.DisplayTextWithKeyword,
                    
                    ToolTip = topic.ToolTip
                };
                LbTopics.Items.Add(lbi);
                if (topicItem.IsSelected)
                {
                    lbi.IsSelected = true;
                }
            }
            ScrollSelectedItem();
            //tooltip
            Face.ToolTip = knight.Name;
            //备注
            TBNotes.Text = knight.SpecialTopics;

            _knight = knight;
            LoadGridSearch(knight);
            _initialized = true;
            ShowDialog();
        }

        private void ButtonLocation_OnClick(object sender, RoutedEventArgs e)
        {
            ScrollSelectedItem();
        }

        private void LbTopics_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_initialized) return;
            Thread.Sleep(100);
            TopicChanged?.Invoke(this, e);
        }

        private void ButtonClose_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public void ScrollSelectedItem()
        {
            if (LbTopics.SelectedItems.Count > 0)
            {
                LbTopics.ScrollIntoView(LbTopics.Items[LbTopics.Items.Count - 1]);
                LbTopics.UpdateLayout();
                LbTopics.ScrollIntoView(LbTopics.SelectedItems[0]); LbTopics.ScrollIntoView(LbTopics.SelectedItems[0]);
                Storyboard sb = (Storyboard)FindResource("StoryboardListBoxItem");
                sb.Begin(LbTopics.SelectedItems[0] as ListBoxItem);
            }
        }

        private void Grid_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void LoadGridSearch(Knight knight)
        {
            foreach (var control in GridSearch.Children)
            {
                if (control is ComboBox)
                {
                    ComboBox cb = control as ComboBox;
                    if (cb.Name.StartsWith("CBTopic"))
                    {
                        cb.Items.Add("");
                        cb.Items.Add(Const.TopicName.Cooking);
                        cb.Items.Add(Const.TopicName.Fashion);
                        cb.Items.Add(Const.TopicName.Game);
                        cb.Items.Add(Const.TopicName.Love);
                        cb.Items.Add(Const.TopicName.Quest);
                        cb.Items.Add(Const.TopicName.Train);
                    }
                    if (cb.Name.StartsWith("CBKeyword"))
                    {
                        if (Eirlys.Key.Equals(knight.BaseKey))
                        {
                            cb.Items.Add("");
                            cb.Items.Add(Const.KeywordName.Eirlys_LA);
                            cb.Items.Add(Const.KeywordName.Eirlys_LL);
                            cb.Items.Add(Const.KeywordName.Eirlys_RW);
                            cb.Items.Add(Const.KeywordName.Eirlys_SS);
                            cb.Items.Add(Const.KeywordName.Eirlys_XL);
                            cb.Items.Add(Const.KeywordName.Eirlys_YX);
                        }
                        else if (Dai.Key.Equals(knight.BaseKey))
                        {
                            cb.Items.Add("");
                            cb.Items.Add(Const.KeywordName.Dai_CH);
                            cb.Items.Add(Const.KeywordName.Dai_KZ);
                            cb.Items.Add(Const.KeywordName.Dai_RM);
                        }
                        else
                        {
                            TBKeyword.Visibility = Visibility.Collapsed;
                            cb.Visibility = Visibility.Collapsed;
                        }
                    }
                }
            }
        }

        private void CBSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string topicTarget = TUtil.Common.GetObjectString(CBTopicTarget.SelectedItem);
            string topicNext1 = TUtil.Common.GetObjectString(CBTopicNext1.SelectedItem);
            string topicNext2 = TUtil.Common.GetObjectString(CBTopicNext2.SelectedItem);
            string keywordTarget = TUtil.Common.GetObjectString(CBKeywordTarget.SelectedItem);
            string keywordNext1 = TUtil.Common.GetObjectString(CBKeywordNext1.SelectedItem);
            string keywordNext2 = TUtil.Common.GetObjectString(CBKeywordNext2.SelectedItem);


            System.Diagnostics.Debug.WriteLine("Search Changed:" + $"topicTarget:{topicTarget};topicNext1:{topicNext1};topicNext2:{topicNext2}\n" +
                $"keywordTarget:{keywordTarget};keywordNext1:{keywordNext1};keywordNext2:{keywordNext2}");

            ParagraphSearchResult.Inlines.Clear();

            if (string.IsNullOrEmpty(topicTarget) && string.IsNullOrEmpty(keywordTarget))
            {
                //搜索目标为空,取消
                return;
            }

            foreach (Topic topic in _knight.Topics)
            {
                if (topic.Name.Contains(topicTarget) && topic.Keyword.Contains(keywordTarget)
                    && topic.Next.Name.Contains(topicNext1) && topic.Next.Keyword.Contains(keywordNext1)
                    && topic.Next.Next.Name.Contains(topicNext2) && topic.Next.Next.Keyword.Contains(keywordNext2)
                    )
                {

                    ParagraphSearchResult.Inlines.Add(new Run(topic.DisplayTextWithKeyword) { Foreground = Brushes.LawnGreen });
                    ParagraphSearchResult.Inlines.Add(new LineBreak());
                    if (!string.IsNullOrEmpty(topicNext1) || !string.IsNullOrEmpty(keywordNext1))
                    {
                        ParagraphSearchResult.Inlines.Add(new Run(topic.Next.DisplayTextWithKeyword) { Foreground = Brushes.LightGreen });
                        ParagraphSearchResult.Inlines.Add(new LineBreak());
                        if (!string.IsNullOrEmpty(topicNext2) || !string.IsNullOrEmpty(keywordNext2))
                        {
                            ParagraphSearchResult.Inlines.Add(new Run(topic.Next.Next.DisplayTextWithKeyword) { Foreground = Brushes.YellowGreen });
                            ParagraphSearchResult.Inlines.Add(new LineBreak());
                        }
                    }
                    ParagraphSearchResult.Inlines.Add(new LineBreak());
                    //System.Diagnostics.Debug.WriteLine("Find:" + topic.DisplayTextWithKeyword);
                }
            }
        }
    }
}
