﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Globalization;


namespace CookingRuler
{

    public class RulerControl : FrameworkElement
    {

        public double TickGap       //刻度间距
        {
            get { return ActualWidth / (100 / SmallTickInterval); }
        }


        public double SmallTickInterval     //小刻度间隔
        {
            get { return (double)GetValue(SmallTickIntervalProperty); }
            set
            {
                SetValue(SmallTickIntervalProperty, value);
            }
        }

        public static readonly DependencyProperty SmallTickIntervalProperty =
            DependencyProperty.Register("SmallTickInterval", typeof(double), typeof(RulerControl),
            new FrameworkPropertyMetadata(5d, FrameworkPropertyMetadataOptions.AffectsRender, SmallTickIntervalChanged));

        private static void SmallTickIntervalChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as RulerControl).InvalidateVisual();
        }

        public double SmallTickWidth        //小刻度宽度
        {
            get { return (double)GetValue(SmallTickWidthProperty); }
            set
            {
                SetValue(SmallTickWidthProperty, value);
            }
        }

        public static readonly DependencyProperty SmallTickWidthProperty =
            DependencyProperty.Register("SmallTickWidth", typeof(double), typeof(RulerControl),
            new FrameworkPropertyMetadata(1d, FrameworkPropertyMetadataOptions.AffectsRender, SmallTickWidthChanged));

        private static void SmallTickWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as RulerControl).InvalidateVisual();
        }

        public double SmallTickHeight       //小刻度高度
        {
            get { return (double)GetValue(SmallTickHeightProperty); }
            set
            {
                SetValue(SmallTickHeightProperty, value);
            }
        }

        public static readonly DependencyProperty SmallTickHeightProperty =
            DependencyProperty.Register("SmallTickHeight", typeof(double), typeof(RulerControl),
            new FrameworkPropertyMetadata(4d, FrameworkPropertyMetadataOptions.AffectsRender, SmallTickHeightChanged));

        private static void SmallTickHeightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as RulerControl).InvalidateVisual();
        }

        public Brush SmallTickColor     //小刻度颜色
        {
            get { return (Brush)GetValue(SmallTickColorProperty); }
            set
            {
                SetValue(SmallTickColorProperty, value);
            }
        }

        public static readonly DependencyProperty SmallTickColorProperty =
            DependencyProperty.Register("SmallTickColor", typeof(Brush), typeof(RulerControl),
            new FrameworkPropertyMetadata(new SolidColorBrush(Colors.White), FrameworkPropertyMetadataOptions.AffectsRender, SmallTickColorChanged));

        private static void SmallTickColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as RulerControl).InvalidateVisual();
        }

        public double TickStartOffset       //刻度起始值
        {
            get { return (double)GetValue(TickStartOffsetProperty); }
            set
            {
                SetValue(TickStartOffsetProperty, value);
            }
        }

        public static readonly DependencyProperty TickStartOffsetProperty =
            DependencyProperty.Register("TickStartOffset", typeof(double), typeof(RulerControl),
            new FrameworkPropertyMetadata(0d, FrameworkPropertyMetadataOptions.AffectsRender, TickStartOffsetChanged));

        private static void TickStartOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as RulerControl;
            obj.InvalidateVisual();
        }

        public double TickEndOffset     //刻度结束值
        {
            get { return (double)GetValue(TickEndOffsetProperty); }
            set
            {
                SetValue(TickEndOffsetProperty, value);
            }
        }

        public static readonly DependencyProperty TickEndOffsetProperty =
            DependencyProperty.Register("TickEndOffset", typeof(double), typeof(RulerControl),
            new FrameworkPropertyMetadata(100d, FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(TickEndOffsetChanged)));

        private static void TickEndOffsetChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var obj = sender as RulerControl;
            obj.SmallTickInterval = 5;
            obj.InvalidateVisual();
        }


        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);
            double rulerLength = ActualWidth - 2;
            double gap = rulerLength / 20;
            double currentLength = 1;
            Pen small = new Pen(Brushes.Red, 1);
            GuidelineSet guidelines = new GuidelineSet();
            while (currentLength <= rulerLength)
            {
                guidelines.GuidelinesX.Add(currentLength-1);
                dc.DrawLine(small, new Point(currentLength, 0), new Point(currentLength, ActualHeight));
                currentLength += gap;

            }
            dc.PushGuidelineSet(guidelines);
            dc.DrawLine(small, new Point(ActualWidth - 1, 0), new Point(ActualWidth, ActualHeight));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="curPixel">当前刻度坐标</param>
        /// <param name="curNumber">当前刻度值</param>
        /// <param name="tickNumber">刻度序号</param>
        /// <param name="drawingContext"></param>
        private void DrawLine(double curPixel, double curNumber, int tickNumber, ref int bigTickNumber, DrawingContext drawingContext)
        {
            //FormattedText ft = new FormattedText(
            //            (curNumber).ToString(CultureInfo.CurrentCulture),
            //             CultureInfo.CurrentCulture,
            //             FlowDirection.LeftToRight,
            //             new Typeface("Arial"),
            //             ActualHeight * 3 / 10,
            //             Brushes.DimGray);
            //ft.SetFontWeight(FontWeights.Bold);
            //ft.TextAlignment = TextAlignment.Center;

            Pen small = new Pen(SmallTickColor, SmallTickWidth);
            drawingContext.DrawLine(small, new Point(curPixel, ActualHeight - SmallTickHeight), new Point(curPixel, ActualHeight));     //画小刻度
        }
    }
}
