﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TUtil
{
    public class BaseAppStatus
    {
        public DateTime LastStartupTime { get; set; }
        public DateTime LastCloseTime { get; set; }
    }
}
