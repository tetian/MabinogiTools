﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TUtil
{
    public interface IMessageListener
    {

        /// <summary>
        /// 捕获消息对象,根据消息名进行相应的处理
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageParameter"></param>
        void HandleMessage(object message, object messageParameter);

    }

    /// <summary>
    /// 当消息发送时, 通知任何注册的MessageListener
    /// </summary>
    public class NotificationCenter
    {

        /// <summary>
        /// 存放已经注册的消息
        /// </summary>
        private static readonly Hashtable RegisteredListeners = new Hashtable();

        /// <summary>
        /// 私有构造,不允许实例化
        /// </summary>
        private NotificationCenter()
        {

        }

        /// <summary>
        /// 注册消息
        /// </summary>
        /// <param name="messageListened"></param>
        /// <param name="messageListener"></param>
        public static void RegisterListener(object messageListened, IMessageListener messageListener)
        {
            RegisterListener(messageListened, messageListener, false);
        }

        /// <summary>
        /// 注册消息
        /// </summary>
        /// <param name="messageListened"></param>
        /// <param name="messageListener"></param>
        /// <param name="mutiable"></param>
        public static void RegisterListener(object messageListened, IMessageListener messageListener, bool mutiable)
        {
            if (messageListened == null)
            {
                throw new Exception("messageListener is null ....");
            }
            if (RegisteredListeners.Contains(messageListened))
            {
                RemoveListener(messageListened);
            }
            ArrayList listeners = (ArrayList)RegisteredListeners[messageListened] ?? new ArrayList();
            if (!mutiable)
            {
                listeners.Clear();
            }
            listeners.Add(messageListener);
            RegisteredListeners.Add(messageListened, listeners);

        }

        /// <summary>
        /// 通知侦听程序,消息已经发出.
        /// </summary>
        /// <param name="messageListened"></param>
        /// <param name="messageParameter"></param>
        public static void NotifyListener(object messageListened, object messageParameter)
        {
            ArrayList listeners = (ArrayList)RegisteredListeners[messageListened];
            if (listeners != null && listeners.Count > 0)
            {
                foreach (IMessageListener messageListener in listeners)
                {
                    messageListener.HandleMessage(messageListened, messageParameter);
                }
            }
        }

        /// <summary>
        /// 删除一条消息
        /// </summary>
        /// <param name="messageListened"></param>
        public static void RemoveListener(object messageListened)
        {
            RegisteredListeners.Remove(messageListened);
        }

        /// <summary>
        /// 删除消息侦听程序.
        /// </summary>
        /// <param name="messageListener"></param>
        public static void RemoveListener(IMessageListener messageListener)
        {
            if (messageListener != null)
            {
                ArrayList akeys = new ArrayList(RegisteredListeners.Keys);
                akeys.Sort();
                foreach (string skey in akeys)
                {
                    var listeners = (ArrayList)RegisteredListeners[skey];
                    listeners.Remove(messageListener);
                    // registeredListeners.Add(message, listeners);
                }
            }
        }
    }
}
