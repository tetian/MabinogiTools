﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace TUtil
{
    public static class XmlHelper
    {
        #region 反序列化

        public static T XmlDeserialize<T>(string s, Encoding encoding = null)
        {
            if (string.IsNullOrEmpty(s))
                throw new ArgumentNullException("s");
            if (encoding == null)
                encoding = new UTF8Encoding();

            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream(encoding.GetBytes(s)))
            {
                using (StreamReader sr = new StreamReader(ms, encoding))
                {
                    return (T)mySerializer.Deserialize(sr);
                }
            }
        }

        public static T XmlDeserializeFromFile<T>(string path, Encoding encoding = null)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentNullException("path");
            if (encoding == null)
                encoding = new UTF8Encoding();

            string xml = File.ReadAllText(path, encoding);
            return XmlDeserialize<T>(xml, encoding);
        }
        #endregion

        #region 序列化     

        public static void SerializerToFile<T>(T t, string file)
        {
            string path = Path.GetDirectoryName(file);
            if (string.IsNullOrEmpty(path)) return;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            //string fullPath = string.Format(@"{0}{1}", path, fullName);
            XmlSerializer xmlSerializer = new XmlSerializer(t.GetType());

            using (FileStream stream = new FileStream(file, FileMode.Create))
            {
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stream, new UTF8Encoding(false)))
                {
                    xmlTextWriter.Formatting = Formatting.Indented;
                    xmlSerializer.Serialize(xmlTextWriter, t, ns);
                    xmlTextWriter.Flush();
                    xmlTextWriter.Close();
                }
            }
        }

        public static string Serialize<T>(T t)
        {
            if (null != t)
            {
                return "";
            }
            XmlSerializer xmlSerializer = new XmlSerializer(t.GetType());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, new UTF8Encoding(false)))
                {
                    xmlTextWriter.Formatting = Formatting.Indented;
                    xmlSerializer.Serialize(xmlTextWriter, t);
                    xmlTextWriter.Flush();
                    xmlTextWriter.Close();
                    UTF8Encoding uTF8Encoding = new UTF8Encoding(false, true);
                    return uTF8Encoding.GetString(memoryStream.ToArray());
                }
            }
        }
        #endregion
    }
}
