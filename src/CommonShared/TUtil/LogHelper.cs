﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TUtil
{
    class LogHelper
    {
        public static string Debug(object msg)
        {
            string log = "[" + DateTime.Now + "]" + msg;
            System.Diagnostics.Debug.WriteLine(log);
            return log;
        }

        public static void WriteFile(object msg, string file)
        {
            string log = Debug(msg);
            WriteFileWithoutTime(msg, file);
        }
        public static void WriteFileWithoutTime(object msg, string file)
        {
            string log = msg?.ToString();
            using (FileStream fsWrite = new FileStream(file, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter st = new StreamWriter(fsWrite))
                {
                    st.WriteLine(log);
                    st.Close();
                    st.Dispose();
                    fsWrite.Close();
                    fsWrite.Dispose();
                }
            }
        }
    }
}
