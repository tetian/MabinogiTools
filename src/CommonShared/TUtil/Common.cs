﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace TUtil
{
    public class Common
    {
        public static string GetObjectString(object obj)
        {
            if (obj == null) return string.Empty;
            return obj.ToString();
        }

        private static Color DarkerColor(Color color, float correctionfactory = 50f)
        {
            const float hundredpercent = 100f;
            return Color.FromRgb((byte)(((float)color.R / hundredpercent) * correctionfactory),
                (byte)(((float)color.G / hundredpercent) * correctionfactory),
                (byte)(((float)color.B / hundredpercent) * correctionfactory));
        }

        private static Color LighterColor(Color color, float correctionfactory = 50f)
        {
            correctionfactory = correctionfactory / 100f;
            const float rgb255 = 255f;
            return Color.FromRgb((byte)((float)color.R + ((rgb255 - (float)color.R) * correctionfactory)),
                (byte)((float)color.G + ((rgb255 - (float)color.G) * correctionfactory)),
                (byte)((float)color.B + ((rgb255 - (float)color.B) * correctionfactory)));
        }

        public static Random randomSeek = new Random();

        public static Color GetRandomColor(Color baseColor)
        {
            int intR = randomSeek.Next(256);
            int intG = randomSeek.Next(256);
            int intB = (intR + intG > 400) ? 0 : 400 - intR - intG;
            if (intR == baseColor.R && intG == baseColor.G && intB == baseColor.B)
            {
                return LighterColor(baseColor);
            }
            return Color.FromRgb((byte)intR, (byte)intG, (byte)intB);
        }
        public static Color GetRandomColor(string baseColor)
        {
            Color color = (Color)ColorConverter.ConvertFromString(baseColor);
            return GetRandomColor(color);
        }
    }
}
