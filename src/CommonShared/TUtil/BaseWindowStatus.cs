﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TUtil
{
    public class BaseWindowStatus
    {
        public double Left { get; set; }
        public double Top { get; set; }
        public bool Topmost { get; set; }
    }
}
