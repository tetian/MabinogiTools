﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TUtil
{
    public class TCommond
    {

        public static string AddHeader(string param, string header = "--")
        {
            return header + param;
        }

        private Dictionary<string, string> _commondDict = new Dictionary<string, string>();

        public TCommond(string[] args)
        {
            Load(args);
        }

        public void Load(string[] args)
        {
            foreach (var arg in args)
            {
                string[] kv = arg.Split('=');
                if (1.Equals(kv.Length))
                {
                    _commondDict.Add(kv[0].Replace("--", string.Empty), string.Empty);
                }
                if (1 < kv.Length)
                {
                    _commondDict.Add(kv[0].Replace("--", string.Empty), kv[1]);
                }
            }
        }

        public bool Contain(string name)
        {
            return _commondDict.ContainsKey(name);
        }

        public string Get(string name)
        {
            return _commondDict.ContainsKey(name) ? _commondDict[name] : string.Empty;
        }

        public string this[string name]
        {
            get
            {
                if (!_commondDict.ContainsKey(name)) return string.Empty;
                return _commondDict[name];
            }
            set
            {
                _commondDict[name] = value;
            }
        }

        public static bool DoubleIsZero(double d)
        {
            return double.IsNaN(d) || .0.Equals(d);
        }
    }
}
